import requests

def get_page(url,i):
	r = requests.get(url)
	dic = r.json()
	print("\npage #{:d}".format(i),end='')	
	jason = dic['items']
	pd = {"count" : 0 , "tags" : []}
	count = 0
	try:
		page = []
		for tag in jason:
			page.append(tag['name'])
			print(".",end='')
			count += 1
	except:
		print("error getting page #{:d}".format(i))
	print()
	pd['tags'] = page
	pd['count'] = count
	return pd

def furl(base,page,site,sort="popular",order="desc",minimun=1000):
	varpage = "page={:d}".format(page)
	varsort = "sort="+sort
	varorder = "order="+order
	varmin = "min={:d}".format(minimun)
	varsite = "site="+site
	varsize = "pagesize=100"
	
	#		b	 p    o    m    s    s    s
	url = "{:s}?{:s}&{:s}&{:s}&{:s}&{:s}&{:s}".format(base,varpage,varorder,varmin,varsort,varsite,varsize)
	return url


def main():
	# backup = "https://api.stackexchange.com/2.2/tags?page={:d}&order={:s}&min={:d}&sort=popular&site=stackoverflow".format(page,order,minimun)
	
	base = "https://api.stackexchange.com/2.2/tags"
	site = "stackoverflow"
	pages = []
	n = 20
	print("GETTING {:d} PAGES".format(n))
	n+=1
	for i in range(1,n):
		url = furl(base,i,site)
		page = get_page(url,i)
		pages.append(page)
		
	#arq_name = "tags_{:s}.txt".format(inname)
	with open("tags.txt",'w') as arq:
		for page in pages:
			for tag in page["tags"]:
				towrite = tag + "\n"
				arq.write(towrite)
			
if __name__ == "__main__":
	main()

