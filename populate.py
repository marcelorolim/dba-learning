import psycopg2

def main():
	db = "talentos"
	usr = "postgres"
	pwd = "unipe"
	filename = "habilidades.txt"
	sproc = "add_habs_array"
	#sproc = input("nome da procedure: ")
	#filename = input("filename: ")

	with open(filename,'r') as arq:
		r = arq.read()

	tags = r.split("\n")
	tags.pop(-1)

	print("Adicionando:")
	for tag in tags:
		print(tag)
	with psycopg2.connect(dbname=db,user=usr,password=pwd) as conn:
		with conn.cursor() as cur:
			try:
				cur.callproc("{:s}".format(sproc),([tags]))
				#só tags sem os [] psycopg interpreta cada elemento da lista como uma argumento da procedure
			except Exception as E:
				print(E)
			#finally:
			#	print("{:} adicionadas com sucesso".format(filename.split(".")[0]))

if __name__ == "__main__":
	main()
